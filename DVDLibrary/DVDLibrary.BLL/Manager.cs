﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVDLibrary.Data.Factory;
using DVDLibrary.Data.Interfaces;
using DVDLibrary.Models;
using DVDLibrary.Models.ErrorHandling;

namespace DVDLibrary.BLL
{
    public class Manager
    {
        private IRepo _repo;

        public Manager()
        {
            _repo = RepositoryFactory.GetRepo();
        }

        public Response<List<DVD>> ListDvds()
        {
            var response = new Response<List<DVD>>();
            try
            {
                response.Data = _repo.ListDvds();
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "An error has occured";
                ErrorLog log = new ErrorLog();
                log.Log(ex.ToString());
            }
            return response;
        }

        public void AddDvd(DVD dvd)
        {
            var url = "http://www.imdb.com/find?ref_=nv_sr_fn&q=";
            var urlBack = "&s=all";
            try
            {
                var splitTitle = dvd.Title.Split(' ');
                foreach (var word in splitTitle)
                {
                    url += word + "+";
                }
                url = url.Substring(0, url.Length - 1) + urlBack;
            }
            catch
            {
                url = url + dvd.Title + urlBack;
            }
            finally
            {
                dvd.Website = url;
                _repo.AddDvd(dvd);
            }
            
        }

        public Response<DVD> SetRatingNames(DVD dvd)
        {
            var response = new Response<DVD>();
            try
            {
                if (dvd.Rating.RatingId == 0)
                {
                    dvd.Rating.RatingName = "NA";
                }
                if (dvd.Rating.RatingId == 1)
                {
                    dvd.Rating.RatingName = "G";
                }
                else if (dvd.Rating.RatingId == 2)
                {
                    dvd.Rating.RatingName = "PG";
                }
                else if (dvd.Rating.RatingId == 3)
                {
                    dvd.Rating.RatingName = "PG-13";
                }
                else if (dvd.Rating.RatingId == 4)
                {
                    dvd.Rating.RatingName = "R";
                }
                else if (dvd.Rating.RatingId == 5)
                {
                    dvd.Rating.RatingName = "NR-17";
                }
                response.Data = dvd;
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "An error has occured";
                ErrorLog log = new ErrorLog();
                log.Log(ex.ToString());
            }
            return response;
            
        }

        public void EditDvd(DVD dvd)
        {
            var response = new Response<DVD>();
            try
            { 
                _repo.EditDvd(dvd);
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "An error has occured";
                ErrorLog log = new ErrorLog();
                log.Log(ex.ToString());
            }
        }

        public void RemoveDvd(int id)
        {
            var response = new Response<DVD>();
            try
            {
                _repo.RemoveDvd(id);
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "An error has occured";
                ErrorLog log = new ErrorLog();
                log.Log(ex.ToString());
            }
        }

        public Response<DVD> LoadDvd(int id)
        {
            var response = new Response<DVD>();
            try
            {
                response.Data = _repo.GetDvd(id);
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "An error has occured";
                ErrorLog log = new ErrorLog();
                log.Log(ex.ToString());
            }
            return response;
        }
    }
}
